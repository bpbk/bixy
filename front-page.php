<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Bixy
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		if ( have_posts() ) :

			while ( have_posts() ) :
				the_post(); ?>
				<div class="content home-content">
					<div class="row align-center">
						<div class="col-4 bixy-blue home-hero-content fade-in fade-two">
							<?php the_content(); ?>
						</div>
						<div class="col-8">
							<?php
							if ( $video = get_field('home_video', get_option('page_on_front')) ) { ?>
								<div class="video-container fade-in fade-one">
									<video id="homepage-video" playsinline
										poster="<?php echo get_field('home_video_placeholder', get_option('page_on_front'))['sizes']['large']; ?>">
					          <source src="<?php echo $video['url']; ?>">
					          <img src="<?php echo get_field('home_video_placeholder', get_option('page_on_front'))['sizes']['large']; ?>" alt="">
					        </video>
									<div class="play-button-container">
										<div class="play-button">

										</div>
									</div>
								</div>
							<?php
							} ?>
						</div>
					</div>
					<div class="row pt-2 download-row fade-in fade-three">
						<div class="col-4 flex align-center">
							
						</div>
						<div class="col-8 flex justify-center align-center">
							<a class="app-download-link" href="https://itunes.apple.com/us/app/bixy/id1182318535?mt=8">
								<img src="<?php echo get_template_directory_uri(); ?>/images/appstore-icon.png"/>
							</a>
							<a class="app-download-link" href="https://play.google.com/store/apps/details?id=consumer.bixy.com.bixyconsumer&hl=en">
								<img src="<?php echo get_template_directory_uri(); ?>/images/googleplay-icon.png"/>
							</a>
						</div>
					</div>
				</div>

			<?php
			endwhile;

		endif;
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
