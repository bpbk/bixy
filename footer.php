<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Bixy
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer py-3 bg-light-grey">
		<div class="content">
			<div id="footer-menus">
				<div class="row">
					<div class="col-3">
						<?php
						wp_nav_menu( array(
							'menu'        => 'footer-1',
						) ); ?>
					</div>
					<div class="col-3">
						<?php
						wp_nav_menu( array(
							'menu'        => 'footer-2',
						) ); ?>
					</div>
					<div class="col-3">
						<?php
						wp_nav_menu( array(
							'menu'        => 'footer-3',
						) ); ?>
					</div>
				</div>
			</div>
			<div id="footer-social-links" class="flex">
				<a target="_blank" href="https://facebook.com/teambixy" class="social-link flex justify-center align-center">
					<i class="fab fa-facebook-f"></i>
				</a>
				<a target="_blank" href="https://twitter.com/TeamBixy" class="social-link flex justify-center align-center">
					<i class="fab fa-twitter"></i>
				</a>
				<a target="_blank" href="https://www.instagram.com/teambixy/" class="social-link flex justify-center align-center">
					<i class="fab fa-instagram"></i>
				</a>
				<a target="_blank" href="https://youtube.com/" class="social-link flex justify-center align-center">
					<i class="fab fa-you-tube"></i>
				</a>
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
