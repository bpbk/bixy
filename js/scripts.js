import $ from 'jquery';
// import 'slick-carousel';

if ( $('.play-button-container').length ) {
  $('.play-button-container').each( (i, item) => {
    $(item).on('click', () => {
      const video = $(item).parent().find('video');
      $(item).addClass('hidden');
      video[0].play();
      video[0].setAttribute("controls","controls")  
    })
  })
}

$('.hamburger').on('click', function () {
  $(this).toggleClass('is-active');
  // $('#site-navigation').toggleClass('active')
  return
})
